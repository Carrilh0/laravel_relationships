<?php



Route::get('one-to-one','OneToOneController@oneToOne');
Route::get('one-to-one-inverse','OneToOneController@oneToOneInverse');
Route::get('one-to-one-insert','OneToOneController@OneToOneInsert');


Route::get('one-to-many', 'OneToManyController@OneToMany');

Route::get('/', function () {
    return view('welcome');
});
