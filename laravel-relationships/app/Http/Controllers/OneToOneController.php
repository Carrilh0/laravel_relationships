<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Country;
use App\Models\Location;

class OneToOneController extends Controller
{
    public function oneToOne(){
        //$country = Country::find(2); //ENCONTRA UM PAIS PELA MODEL COM O ID 2
        
        //ENCONTRA UM PAIS PELA MODEL COM O NOME       
        $country = Country::where('name','Brasil')->get()->first();
        
        //$location = $country->location;     //CHAMADA POR ATRIBUTO
        
        
        //CHAMADA POR METODO
        $location = $country->location()->get()->first();
        echo $country->name;
        
        echo '<hr> Longitude: '.$location->longitude.'</hr>';
        echo '<hr> Latitude: '.$location->latitude.'</hr>';
    }

    public function oneToOneInverse(){

        $latitude = 123;    //parametros de latitudes cadastrados no banco
        $longitude = 456;   //parametros de longitude cadastrados no banco

        $location = Location::where('latitude',$latitude)->where('longitude',$longitude)->get()->first(); //filtro para achar valor de acordo com a latitude e longitude

        //CHAMADA POR ATRIBUTO
        $country = $location->country; //referencia a function da model para retornar dados de outra tabela

        echo $country->name; //printa o nome do país baseado no id

    }

    public function OneToOneInsert(){
        
        $dataForm = [
            'name' => 'Alemanha',
            'latitude' => 111,
            'longitude' => 222,
        ];

        $country = Country::create($dataForm);

        //$dataForm['country_id'] = $country->id;
        //$location = Location::create($dataForm);
        

        //METODO ALTERNATIVO

        //$location = new Location;
        //$location->latitude = $dataForm['latitude'];
        //$location->longitude = $dataForm['longitude'];
        //$location->country_id = $country->id;
        //$saveLocation$location->save();

        //METODO ALTERNATIVO 2
        //$location = $country->location()->create($dataForm);
    }
}
